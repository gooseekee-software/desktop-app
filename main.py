import sys
from app import desktop_app

if __name__ == '__main__':
    app = desktop_app.App(sys.argv)
    r = app.run()
    exit(r)
