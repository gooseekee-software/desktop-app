import os
import time

from PyQt5.QtCore import QThread
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication, QSplashScreen
from PyQt5 import QtCore
from app.windows.main_window import MainWindow
from app.windows.order_details_window import OrderDetailsWindow
from app.windows.driver_list_window import DriverListWindow


class App(QApplication):
    def __init__(self, *args):
        super().__init__(*args)
        self.setApplicationName('GaaGaa operator')

        # show splash for at least 3 seconds
        self.splash = QSplashScreen(pixmap=QPixmap(os.path.join("app", "res", "splash.jpg")))
        self.splash.show()

        # setup main window
        self.main_window = MainWindow()
        self.main_window.details_request_signal.connect(self.show_order_details)

        # setup details window
        self.order_details_window = OrderDetailsWindow()

        # setup driver list window
        self.driver_list_window = DriverListWindow()

    def show_windows(self):
        # finish splash screen
        self.splash.finish(self.main_window)

        # show windows
        self.main_window.show()
        self.order_details_window.show()
        self.driver_list_window.show()

    def run(self):
        # run main event loop
        QtCore.QTimer.singleShot(1000, self.show_windows)
        self.exec()

    @QtCore.pyqtSlot(str)
    def show_order_details(self, order_id):
        self.order_details_window.show_details(order_id)
