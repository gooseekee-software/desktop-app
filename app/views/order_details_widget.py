from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QLabel, QWidget, QPushButton
from PyQt5.QtWidgets import QGridLayout, QComboBox
import app.net.connection as conn


def get_drivers():
    drivers = []
    for driver in conn.get_driver_list():
        drivers.append(str(driver['driver_id']) + '. ' + str(driver['name']))
    return drivers


class OrderDetailsView(QWidget):

    def __init__(self, order_id):
        super().__init__()
        self.order_id = order_id
        self.setLayout(QGridLayout())

        self.assign_driver_button = QPushButton("Assign the driver", self)
        self.assign_driver_button.clicked.connect(self.assign_driver_handler)

        order_info = conn.get_order_by_id(order_id)
        self.layout().addWidget(QLabel("Source address"), 1, 1)
        self.layout().addWidget(QLabel(order_info["pickup_address"]), 1, 2)
        self.layout().addWidget(QLabel("Destination address"), 2, 1)
        self.layout().addWidget(QLabel(order_info["delivery_address"]), 2, 2)
        self.layout().addWidget(QLabel("Status"), 3, 1)
        self.layout().addWidget(QLabel(order_info["status"]), 3, 2)
        self.combo = QComboBox(self)
        self.combo.addItems(get_drivers())
        self.layout().addWidget(self.combo, 4, 1)
        self.layout().addWidget(self.assign_driver_button, 4, 2)

    @pyqtSlot()
    def assign_driver_handler(self):
        driver_id = self.combo.itemData(self.combo.currentIndex()).split(".")[0]
        order_id = self.order_id
        # print("Assigning driver {} to {}".format(driver_id, order_id))
        conn.assign_driver(order_id, 2)
