from PyQt5.QtWidgets import QGroupBox, QHBoxLayout, QPushButton
from PyQt5.Qt import pyqtSignal, pyqtSlot


class OrderCard(QGroupBox):
    details_signal = pyqtSignal(str)

    def __init__(self, id, source, destination, status):
        super().__init__()

        self.id = id

        button = QPushButton()
        button.clicked.connect(self.on_button_press)
        button.setText("ID[{}] \nfrom {} \nto {} \n{}".format(id, source, destination, status))

        layout = QHBoxLayout()
        layout.addWidget(button)

        self.setLayout(layout)

    @pyqtSlot()
    def on_button_press(self):
        self.details_signal.emit(str(self.id))
