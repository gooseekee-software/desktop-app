def get_html():
    return html_string_representation


class MapView:
    html_string_representation = ''
    html_header = ''
    html_body = ''
    html_script = ''

    def __init__(self, origin_coordinates, destination_coordinates, driver_id):
        global html_header, html_body, html_script, html_string_representation
        html_header = '''
                <head>
                    <meta charset='utf-8'/>
                    <title></title>
                    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no'/>
                    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.50.0/mapbox-gl.js'></script>
                    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.50.0/mapbox-gl.css' rel='stylesheet'/>
                    <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
                    <style>
                         body {
                         margin: 0;
                         padding: 0;
                         }
                         #map {
                        position: absolute;
                        top: 0;
                        bottom: 0;
                        width: 100%;
                        }
                    </style>
               </head>
               '''
        html_script = '''
                <script>
                     mapboxgl.accessToken = 'pk.eyJ1IjoiYXNhbGlraHp5YW5vdiIsImEiOiJjam5kb2YwaG4wNGUwM2tvdzJpMHliZ2RtIn0.0G1ISnTcD5vAR7EjD3PlAQ';
                     var map = new mapboxgl.Map({
                         container: 'map',
                         style: 'mapbox://styles/mapbox/streets-v10',
                         center: ''' + str(origin_coordinates) + ''',
                         zoom: 12
                         });
                     map.on('load', function() {
                            getRoute();
                            map.loadImage('https://png.icons8.com/metro/1600/truck.png', function(error, image) {
                            if (error) throw error;
                            map.addImage('truck', image);
                            map.addLayer({
                                "id": "points",
                                "type": "symbol",
                                "source": {
                                    "type": "geojson",
                                    "data": {
                                        "type": "FeatureCollection",
                                        "features": [{
                                            "type": "Feature",
                                            "geometry": {
                                                "type": "Point",
                                                "coordinates": ''' + str(origin_coordinates) + '''
                                            }
                                        }]
                                    }
                                },
                                "layout": {
                                    "icon-image": "truck",
                                    "icon-size": 0.025
                                }
                            });
                        });

                         }
                     );
                                        
                     function getRoute() {
                        var start = ''' + str(origin_coordinates) + ''';
                        var end = ''' + str(destination_coordinates) + ''';
                        var directionsRequest = 'https://api.mapbox.com/directions/v5/mapbox/cycling/' + start[0] + ',' + start[1] + ';' + end[0] + ',' + end[1] + '?geometries=geojson&access_token=' + mapboxgl.accessToken;
                        $.ajax({
                             method: 'GET',
                             url: directionsRequest,
                        }).done(function(data) {
                             var route = data.routes[0].geometry;
                             map.addLayer({
                                id: 'route',
                                type: 'line',
                                source: {
                                    type: 'geojson',
                                    data: {
                                        type: 'Feature',
                                        geometry: route
                                    }
                                },
                             paint: {
                                'line-width': 2
                             }
                            });
                        map.addLayer({
                             id: 'start',
                             type: 'circle',
                             source: {
                                 type: 'geojson',
                                 data: {
                                     type: 'Feature',
                                     geometry: {
                                         type: 'Point',
                                         coordinates: start
                                     }
                                 }
                             }
                        });
                        map.addLayer({
                             id: 'end',
                             type: 'circle',
                             source: {
                                 type: 'geojson',
                                 data: {
                                     type: 'Feature',
                                     geometry: {
                                     type: 'Point',
                                     coordinates: end
                                 }
                             }
                         }
                        });
                       });
                     }
                     map.addControl(new MapboxDirections({
                         accessToken: mapboxgl.accessToken
                     }), 'top-left');
                </script>                
                '''
        html_body = '''
                <body>
                    <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v3.1.3/mapbox-gl-directions.js'></script>
                    <link rel='stylesheet'
                        href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v3.1.3/mapbox-gl-directions.css'
                    type='text/css'/>
                    <div id='map'></div>                
                    ''' + html_script + '''
                </body>
                '''
        html_string_representation = '''
                <!DOCTYPE html>
                <html>
                    ''' + html_header + '''
                    ''' + html_body + '''
                </html>
                '''
