import os
import sys

from PyQt5.QtCore import QCoreApplication
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import QMainWindow
from PyQt5.Qt import pyqtSignal

from app.net import connection
from app.views.order_card_widget import OrderCard


class MainWindow(QMainWindow):
    UI_PATH = os.path.join('app', 'ui', 'main_window.ui')
    details_request_signal = pyqtSignal(str)

    def __init__(self):
        super().__init__()
        loadUi(self.UI_PATH, self)
        self.setWindowTitle('GaaGaa operator')
        self.actionRefresh.triggered.connect(self.refresh_list)
        self.actionExit.triggered.connect(self.quit)
        self.refresh_list()

    def refresh_list(self):
        # alo = connection.get_order_list()
        for order in connection.get_order_list():
            card = OrderCard(order["parcel_id"],
                             order["pickup_address"],
                             order["delivery_address"],
                             order["status"])
            card.details_signal.connect(self.request_details)
            self.verticalLayout.addWidget(card)

    def request_details(self, order_id):
        self.details_request_signal.emit(order_id)

    def quit(self, event):
        print("Exit event: {}".format(event))
        QCoreApplication.quit()
