import os

from PyQt5 import QtCore
from PyQt5.QtCore import QObject, pyqtSlot
from PyQt5.QtCore import pyqtSignal
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import QWidget, QLabel

from app.net import connection
from app.views.order_details_widget import OrderDetailsView


class OrderDetailsWindow(QWidget):
    UI_PATH = os.path.join('app', 'ui', 'order_info.ui')

    def __init__(self):
        super().__init__()
        loadUi(self.UI_PATH, self)
        self.setWindowTitle('Order details')
        self.browser.setUrl(QtCore.QUrl.fromLocalFile(os.path.abspath(os.path.join("app", "res", "index.html"))))
        self.tabWidget.currentChanged.connect(self.show_current_order)

    def show_details(self, order_id):
        w_id = self.tabWidget.addTab(OrderDetailsView(order_id), order_id)
        self.tabWidget.setCurrentIndex(w_id)

    def show_location(self, position, start, end, driver_name, source_address, destination_address):
        self.browser.page().runJavaScript("cleanUP();")
        self.browser.page().runJavaScript("addDriver([0, 0]);")
        self.browser.page().runJavaScript("addRoute([{}, {}], [{}, {}]);".format(start[0], start[1], end[0], end[1]))
        self.browser.page().runJavaScript("setLocation([{}, {}]);".format(position[0], position[1]))
        self.browser.page().runJavaScript("setInfo(\"{}\")".format(driver_name))
        self.browser.page().runJavaScript("setAddresses(\"{}\", \"{}\");".format(source_address, destination_address))

    @pyqtSlot()
    def show_current_order(self):
        self.request_map_view(self.tabWidget.currentIndex())

    def request_map_view(self, order_id):
        if order_id == 0:
            return

        print("Showing info about {}".format(order_id))

        order_info = connection.get_order_by_id(order_id)

        if True:  # "driver_id" in order_info.keys():
            driver_info = connection.get_driver_by_id(1)
            position = [driver_info["longitude"], driver_info["latitude"]]
            delivery_coordinates = order_info["delivery_coordinates"]
            source = order_info["delivery_address"]
            destination = order_info["pickup_address"]
            driver_name = driver_info['name']
            # pickup_coordinates = order_info["pickup_coordinates"]
            end = [delivery_coordinates["latitude"], delivery_coordinates["longitude"]]
            print(end)
            # start = [pickup_coordinates["latitude"], pickup_coordinates["longitude"]]

            self.show_location(position, [48.74167, 55.75289], end, driver_name, source, destination)
