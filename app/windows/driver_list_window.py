import os

from PyQt5.uic import loadUi
from PyQt5.QtWidgets import QWidget

from app.net import connection
from app.views.driver_card_widget import DriverCard


class DriverListWindow(QWidget):
    ui_path = os.path.join('app', 'ui', 'driver_list.ui')

    def __init__(self):
        super().__init__()
        loadUi(self.ui_path, self)
        self.setWindowTitle('Driver list')
        self.update_driver_list()

    def update_driver_list(self):
        for driver in connection.get_driver_list():
            card = DriverCard(driver)
            self.verticalLayout.addWidget(card)

