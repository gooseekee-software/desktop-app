import urllib.request
import requests
import json


class DriverListRetrievalException(Exception):
    pass


class OrderListRetrievalException(Exception):
    pass


def get_order_list():
    with urllib.request.urlopen("https://gaagaa-backend.herokuapp.com/operator/list_deliveries") as url:
        r = url.read().decode()
        j = json.loads(r)
        status = j["status"]
        if status == "success":
            orders = j["deliveries"]
            return orders
        else:
            raise OrderListRetrievalException("Failed to retrieve order list: {}".format(j["message"]))


def get_driver_list():
    with urllib.request.urlopen("https://gaagaa-backend.herokuapp.com/operator/map") as url:
        r = url.read().decode()
        j = json.loads(r)
        status = j["status"]
        if status == "success":
            # FIXME backend should return a list insted of a string
            return json.loads(j["drivers"])
        else:
            raise DriverListRetrievalException("Failed to retrieve driver list: {}".format(j["message"]))


def get_driver_by_id(driver_id):
    driver_list = get_driver_list()
    for driver in driver_list:
        if driver["driver_id"] == int(driver_id):
            return driver
    raise DriverListRetrievalException("Driver with id {} was not found in {}.".format(driver_id, driver_list))


def get_order_by_id(order_id):
    order_list = get_order_list()
    for order in order_list:
        if order["parcel_id"] == int(order_id):
            return order
    raise OrderListRetrievalException("Order with id {} was not found in {}.".format(order_id, order_list))


def assign_driver(order_id, driver_id):
    payload = json.dumps({"driver_id": driver_id, "parcel_id": order_id})
    r = requests.post("https://gaagaa-backend.herokuapp.com/operator/assign_driver", payload=payload)
    print(r)
